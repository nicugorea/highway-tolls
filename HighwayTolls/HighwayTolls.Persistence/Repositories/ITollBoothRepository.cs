﻿using HighwayTolls.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace HighwayTolls.Persistence.Repositories
{
    public interface ITollBoothRepository : IRepository<TollBooth>
    {
        IEnumerable<TollBooth> GetTollBoothsByLocationId(int id);
        TollBooth GetTollBoothById(int id);
    }
}
