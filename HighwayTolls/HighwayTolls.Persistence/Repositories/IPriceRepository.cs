﻿using HighwayTolls.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace HighwayTolls.Persistence.Repositories
{
    public interface IPriceRepository : IRepository<Price>
    {
        IEnumerable<Price> GetPricesByLocationId(int id);
    }
}
