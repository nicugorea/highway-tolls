﻿using HighwayTolls.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace HighwayTolls.Persistence.Repositories
{
    public interface ILocationRepository : IRepository<Location>
    {
        Location GetLocationById(int id);
    }
}
