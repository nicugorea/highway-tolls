﻿using HighwayTolls.Domain.Entities;

namespace HighwayTolls.Persistence.Repositories
{
    public interface IVehicleCategoryRepository:IRepository<VehicleCategory>
    {
    }
}
