﻿using System;
using HighwayTolls.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace HighwayTolls.Persistence.EF
{
    public partial class HighwayTollsDbContext : IdentityDbContext<IdentityUser>
    {

        public HighwayTollsDbContext(DbContextOptions<HighwayTollsDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<Payment> Payments { get; set; }
        public virtual DbSet<Price> Prices { get; set; }
        public virtual DbSet<TollBooth> TollBooths { get; set; }
        public virtual DbSet<VehicleCategory> VehicleCategories { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region Employee Configuration

            modelBuilder.Entity<Employee>(entity =>
            {
              
                entity.Property(e => e.Id)
                    .HasMaxLength(255)
                    .ValueGeneratedNever();

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            #endregion

            #region Location Configuration

            modelBuilder.Entity<Location>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            #endregion

            #region TollBooth Configuration

            modelBuilder.Entity<TollBooth>(entity =>
            {
                entity.HasOne(d => d.Location)
                    .WithMany(p => p.TollBooths)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Locations_TollBooths");
            });

            #endregion

            #region Payment Configuration

            modelBuilder.Entity<Payment>(entity =>
            {
                entity.Property(e => e.EmployeeId)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Paid).HasColumnType("money");

                entity.Property(e => e.Price).HasColumnType("money");

                entity.Property(e => e.Time).HasColumnType("datetime");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.Payments)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Payments_Employees");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.Payments)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Payments_Locations");

                entity.HasOne(d => d.TollBooth)
                    .WithMany(p => p.Payments)
                    .HasForeignKey(d => d.TollBoothId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Payments_TollBooths");

                entity.HasOne(d => d.Vehicle)
                    .WithMany(p => p.Payments)
                    .HasForeignKey(d => d.VehicleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Payments_VehicleCategories");
            });

            #endregion

            #region Price Configuration

            modelBuilder.Entity<Price>(entity =>
            {
                entity.HasKey(e => new { e.LocationId, e.VehicleCategoryId });

                entity.Property(e => e.PriceValue).HasColumnType("money");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.Prices)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Prices_Locations");

                entity.HasOne(d => d.VehicleCategory)
                    .WithMany(p => p.Prices)
                    .HasForeignKey(d => d.VehicleCategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Prices_VehicleCategories");
            });

            #endregion

            #region VehicleCategory

            modelBuilder.Entity<VehicleCategory>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            #endregion

        }
    }
}
