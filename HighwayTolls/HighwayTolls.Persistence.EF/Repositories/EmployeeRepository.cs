﻿using HighwayTolls.Domain.Entities;
using HighwayTolls.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace HighwayTolls.Persistence.EF.Repositories
{
    public class EmployeeRepository : Repository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(HighwayTollsDbContext context) : base(context)
        {
        }
    }
}
