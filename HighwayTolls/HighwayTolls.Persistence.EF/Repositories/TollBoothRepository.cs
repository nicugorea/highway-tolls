﻿using HighwayTolls.Domain.Entities;
using HighwayTolls.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HighwayTolls.Persistence.EF.Repositories
{
    public class TollBoothRepository : Repository<TollBooth>, ITollBoothRepository
    {
        public TollBoothRepository(HighwayTollsDbContext context) : base(context)
        {

        }

        public TollBooth GetTollBoothById(int id)
        {
            return _entities.Where(p => p.Id == id).FirstOrDefault();
        }

        public IEnumerable<TollBooth> GetTollBoothsByLocationId(int id)
        {
            return _entities.Where(p => p.LocationId == id);
        }
    }
}
