﻿using HighwayTolls.Domain.Entities;
using HighwayTolls.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HighwayTolls.Persistence.EF.Repositories
{
    public class PriceRepository : Repository<Price>, IPriceRepository
    {
        public PriceRepository(HighwayTollsDbContext context) : base(context)
        {
        }

        public IEnumerable<Price> GetPricesByLocationId(int id)
        {
            return _entities.Where(p => p.LocationId == id);
        }
    }
}
