﻿    using HighwayTolls.Domain.Entities;
using HighwayTolls.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HighwayTolls.Persistence.EF.Repositories
{
    public class LocationRepository : Repository<Location>, ILocationRepository
    {
        public LocationRepository(HighwayTollsDbContext context) : base(context)
        {
        }

        public Location GetLocationById(int id)
        {
            return _entities.Where(p => p.Id == id).FirstOrDefault();   
        }
    }
}
