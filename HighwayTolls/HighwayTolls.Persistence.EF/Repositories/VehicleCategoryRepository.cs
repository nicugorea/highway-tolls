﻿using HighwayTolls.Domain.Entities;
using HighwayTolls.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace HighwayTolls.Persistence.EF.Repositories
{
    public class VehicleCategoryRepository : Repository<VehicleCategory>, IVehicleCategoryRepository
    {
        public VehicleCategoryRepository(HighwayTollsDbContext context) : base(context)
        {
        }
    }
}
