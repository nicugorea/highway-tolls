﻿using HighwayTolls.Domain.Entities;
using HighwayTolls.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace HighwayTolls.Persistence.EF.Repositories
{
    public class PaymentRepository : Repository<Payment>, IPaymentRepository
    {
        public PaymentRepository(HighwayTollsDbContext context) : base(context)
        {
        }
    }
}
