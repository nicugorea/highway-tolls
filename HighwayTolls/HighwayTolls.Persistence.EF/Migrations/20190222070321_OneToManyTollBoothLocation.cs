﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HighwayTolls.Persistence.EF.Migrations
{
    public partial class OneToManyTollBoothLocation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "TollBooths",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TollBooths_LocationId",
                table: "TollBooths",
                column: "LocationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Locations_TollBooths",
                table: "TollBooths",
                column: "LocationId",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Locations_TollBooths",
                table: "TollBooths");

            migrationBuilder.DropIndex(
                name: "IX_TollBooths_LocationId",
                table: "TollBooths");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "TollBooths");
        }
    }
}
