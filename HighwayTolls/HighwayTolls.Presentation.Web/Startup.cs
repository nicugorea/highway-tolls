﻿using HighwayTolls.Application.Implementations;
using HighwayTolls.Application.Interfaces;
using HighwayTolls.Persistence.EF;
using HighwayTolls.Persistence.EF.Repositories;
using HighwayTolls.Persistence.Repositories;
using HighwayTolls.Presentation.Web.AppData;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace HighwayTolls.Presentation.Web
{
    public class Startup
    {
        // Settings stored in appsetings.json
        private readonly IConfiguration Configuration;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<HighwayTollsDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("NetRom")));

            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<HighwayTollsDbContext>();


            services.AddTransient<ILocationRepository, LocationRepository>();
            services.AddTransient<ITollBoothRepository, TollBoothRepository>();
            services.AddTransient<IVehicleCategoryRepository, VehicleCategoryRepository>();
            services.AddTransient<IPriceRepository, PriceRepository>();
            services.AddTransient<IPaymentRepository, PaymentRepository>();
            services.AddTransient<IEmployeeRepository, EmployeeRepository>();


            services.AddTransient<ILocationService, LocationService>();
            services.AddTransient<ITollBoothService, TollBoothService>();
            services.AddTransient<IVehicleCategoryService, VehicleCategoryService>();
            services.AddTransient<IPriceService, PriceService>();
            services.AddTransient<IPaymentService, PaymentService>();
            services.AddTransient<IEmployeeService, EmployeeService>();

            services.AddSingleton<ApplicationData>();

            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = "/login";
            });
                services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                // Show all details of the exception
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // TODO: Error page for user
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            // Default route is /{controlller=Home}/{action={Index}/{id?}
            app.UseMvcWithDefaultRoute();
        }
    }
}
