﻿using System;
using HighwayTolls.Application.Interfaces;
using HighwayTolls.Domain.Entities;
using HighwayTolls.Presentation.Web.AppData;
using HighwayTolls.Presentation.Web.ViewModels.HomeVM;
using HighwayTolls.Presentation.Web.ViewModels.HomeVM.Validations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HighwayTolls.Presentation.Web.Controllers
{
    [Authorize()]
    [Route("/")]
    public class HomeController : Controller
    {
        private readonly IPaymentService _paymentService;
        private readonly IPriceService _priceService;
        private readonly IVehicleCategoryService _vehicleCategoryService;

        private readonly ApplicationData _applicationData;

        public HomeController(IPaymentService paymentService,
            IVehicleCategoryService vehicleCategoryService,
            IPriceService priceService,
            ApplicationData applicationData)
        {
            _paymentService = paymentService;
            _vehicleCategoryService = vehicleCategoryService;
            _priceService = priceService;
            _applicationData = applicationData;
        }

        public IActionResult Index()
        {
            var viewModel = new IndexViewModel(_priceService, 
                _vehicleCategoryService.GetVehicleCategories(), 
                _applicationData.SelectedLocationId.Value);
            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Index(IndexViewModel viewModel)
        {
            var validation = new IndexValidation();
            var validationResult = validation.Validate(viewModel);
            if (validationResult.IsValid)
            {
                var payment = new Payment
                {
                    EmployeeId = _applicationData.CurrentEmployeeId,
                    VehicleId = viewModel.VehicleCategoryId,
                    LocationId = _applicationData.SelectedLocationId.Value,
                    TollBoothId = _applicationData.SelectedTollBoothId.Value,
                    Price = viewModel.Price,
                    Paid = viewModel.Paid,
                    Time = DateTime.Now
                };
                _paymentService.AddPayment(payment);
            }
            else
            {
                foreach (var error in validationResult.Errors)
                {
                    ModelState.AddModelError(error.PropertyName, error.ErrorMessage);
                }
            }
            return View(viewModel);
        }
    }
}