﻿using System;
using System.Threading.Tasks;
using HighwayTolls.Application.Interfaces;
using HighwayTolls.Domain.Entities;
using HighwayTolls.Presentation.Web.AppData;
using HighwayTolls.Presentation.Web.ViewModels.AccountVM;
using HighwayTolls.Presentation.Web.ViewModels.AccountVM.Validations;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace HighwayTolls.Presentation.Web.Controllers
{
    public class AccountController : Controller
    {


        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;

        private readonly ILocationService _locationService;
        private readonly ITollBoothService _tollBoothService;
        private readonly IEmployeeService _employeeService;
        private readonly ApplicationData _applicationData;

        public AccountController(UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager,
            ILocationService locationService,
            ITollBoothService tollBroothService,
            IEmployeeService employeeService,
            ApplicationData applicationData)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _locationService = locationService;
            _tollBoothService = tollBroothService;
            _employeeService = employeeService;
            _applicationData = applicationData;
        }

        [HttpGet("/login")]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost("/login")]
        public async Task<IActionResult> Login(LoginViewModel viewModel)
        {
            var validation = new LoginValidation();
            var validationResult = validation.Validate(viewModel);
            if (validationResult.IsValid)
            {
                var user = await _userManager.FindByNameAsync(viewModel.Username);

                if (user != null)
                {
                    var signInResult = await _signInManager.PasswordSignInAsync(user, viewModel.Password, false, false);
                    if (signInResult.Succeeded)
                    {
                        _applicationData.CurrentEmployeeId = _employeeService.GetEmployeeByIdentityId(user.Id).Id;
                        return RedirectToAction(nameof(Select));
                    }
                }

                ModelState.AddModelError("", "The Username or password is incorect");

            }
            else
            {
                foreach (var error in validationResult.Errors)
                {
                    ModelState.AddModelError(error.PropertyName, error.ErrorMessage);
                }
            }

            return View(viewModel);
        }

        [HttpGet("/register")]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost("/register")]
        public async Task<IActionResult> Register(RegisterViewModel viewModel)
        {
            var validation = new RegisterValidation();
            var validationResult = validation.Validate(viewModel);
            if (validationResult.IsValid)
            {
                var user = new IdentityUser { UserName = viewModel.Username };

                var signInResult = await _userManager.CreateAsync(user, viewModel.Password);
                if (signInResult.Succeeded)
                {
                    var employee = new Employee
                    {
                        Id = Guid.NewGuid().ToString(),
                        FirstName = "",
                        LastName = "",
                        IdentityUserId = user.Id
                    };

                    _employeeService.AddEmployee(employee);
                    return RedirectToAction(nameof(Login));
                }

                ModelState.AddModelError("", "The Username is used");

            }
            else
            {
                foreach (var error in validationResult.Errors)
                {
                    ModelState.AddModelError(error.PropertyName, error.ErrorMessage);
                }
            }

            return View(viewModel);

        }

        [HttpGet("/login/select")]
        public IActionResult Select()
        {
            var viewModel = new SelectViewModel
            {
                Locations = _locationService.GetLocations()
            };

            return View(viewModel);
        }

        [HttpPost("/login/select")]
        public IActionResult Select(SelectViewModel viewModel)
        {
            var validation = new SelectValidation(_locationService, _tollBoothService);
            var validationResult = validation.Validate(viewModel);

            if (validationResult.IsValid)
            {
                _applicationData.SelectedLocationId = viewModel.LocationId;
                _applicationData.SelectedTollBoothId = viewModel.TollBoothId;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                foreach (var error in validationResult.Errors)
                {
                    ModelState.AddModelError(error.PropertyName, error.ErrorMessage);
                }
            }

            return View(viewModel);
        }

        /// <summary>
        /// Request to get a Json Array containing all TollBooths from specified location
        /// </summary>
        /// <param name="id">Id of the Location</param>
        /// <returns>Json Array containing TollBooths filtered by location id</returns>
        [HttpGet("/select/{id}")]
        public JsonResult GetTollBrooths(int id)
        {
            return new JsonResult(_tollBoothService.GetTollBoothsByLocationId(id));
        }


    }
}