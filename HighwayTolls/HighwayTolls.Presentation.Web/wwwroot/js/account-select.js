﻿$("#location").change(function () {
    $("#tollbooths").text("");
    var locationId = $("#location option:selected").attr("value");
    $.get("/select/"+locationId, function (data) {
        for (var i in data) {
            var option = document.createElement("option");
            option.value = data[i].id;
            option.text = data[i].name;
            console.log(data[i]);
            if (data[i].isActive===false) {
                option.disabled = true;
            }
            $("#tollbooths").append(option);
        }
    });
});