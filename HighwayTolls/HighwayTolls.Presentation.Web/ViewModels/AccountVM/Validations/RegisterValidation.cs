﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HighwayTolls.Presentation.Web.ViewModels.AccountVM.Validations
{
    public class RegisterValidation : AbstractValidator<RegisterViewModel>
    {
        public RegisterValidation()
        {
            RuleFor(e => e.Username)
                .NotNull()
                .Length(6, 40);

            RuleFor(e => e.Password)
                .NotNull()
                .Length(6, 40);
        }
    }
}
