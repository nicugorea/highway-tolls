﻿using FluentValidation;
using HighwayTolls.Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HighwayTolls.Presentation.Web.ViewModels.AccountVM.Validations
{
    public class SelectValidation : AbstractValidator<SelectViewModel>
    {
        private readonly ILocationService _locationService;
        private readonly ITollBoothService _tollBoothService;

        public SelectValidation(ILocationService locationService,
            ITollBoothService tollBoothService)
        {
            _locationService = locationService;
            _tollBoothService = tollBoothService;

            RuleFor(e => e.LocationId)
                .NotNull()
                .Must(p =>
                {
                    return _locationService.GetLocationById(p) != null;
                });

            RuleFor(e => e.TollBoothId)
                .NotNull()
                .Must(p =>
                {
                    return _tollBoothService.GetTollBoothById(p) != null;
                });
        }
    }
}
