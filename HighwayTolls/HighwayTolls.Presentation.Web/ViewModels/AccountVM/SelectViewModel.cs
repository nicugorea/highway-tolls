﻿using HighwayTolls.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HighwayTolls.Presentation.Web.ViewModels.AccountVM
{
    public class SelectViewModel
    {
        public int LocationId { get; set; }
        public int TollBoothId { get; set; }

        public IEnumerable<Location> Locations { get; set; }
    }
}
