﻿using HighwayTolls.Application.Interfaces;
using HighwayTolls.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HighwayTolls.Presentation.Web.ViewModels.HomeVM
{
    public class IndexViewModel
    {
        public int VehicleCategoryId { get; set; }
        public decimal Price { get; set; }
        public decimal Paid { get; set; }
        public IEnumerable<VehicleCategory> VehicleCategories { get; private set; }
        public List<Tuple<VehicleCategory, Price>> Prices { get; private set; }

        public IndexViewModel(IPriceService priceService, IEnumerable<VehicleCategory> vehicleCategories, int locationId)
        {
            VehicleCategories = vehicleCategories;
            Prices = new List<Tuple<VehicleCategory, Price>>();
            var prices = priceService.GetPricesByLocationId(locationId).ToList();
            foreach (var item in VehicleCategories)
            {
                Prices.Add(new Tuple<VehicleCategory, Price>
                    (item,
                    item.Prices
                    .Where(p => p.LocationId == locationId)
                    .FirstOrDefault()));
            }
        }

    }
}
