﻿using FluentValidation;

namespace HighwayTolls.Presentation.Web.ViewModels.HomeVM.Validations
{
    public class IndexValidation : AbstractValidator<IndexViewModel>
    {
        public IndexValidation()
        {
            RuleFor(p => p.Price)
                .GreaterThanOrEqualTo(0);

            RuleFor(p => p).Must(p=>p.Paid>=p.Price);

            RuleFor(p => p.VehicleCategoryId);

        }
    }
}
