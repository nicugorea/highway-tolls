﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HighwayTolls.Presentation.Web.AppData
{
    public class ApplicationData
    {
        public string CurrentEmployeeId { get; set; }
        public int? SelectedLocationId { get; set; }
        public int? SelectedTollBoothId { get; set; }
    }
}
