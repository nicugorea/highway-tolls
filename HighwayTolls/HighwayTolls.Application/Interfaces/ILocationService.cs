﻿using HighwayTolls.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace HighwayTolls.Application.Interfaces
{
    public interface ILocationService
    {
        IEnumerable<Location> GetLocations();
        Location GetLocationById(int id);

    }
}
