﻿using HighwayTolls.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace HighwayTolls.Application.Interfaces
{
    public interface IPriceService
    {
        IEnumerable<Price> GetPrices();
        IEnumerable<Price> GetPricesByLocationId(int id);
    }
}
