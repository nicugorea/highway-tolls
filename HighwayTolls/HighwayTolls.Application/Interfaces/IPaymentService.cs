﻿using System;
using System.Collections.Generic;
using System.Text;
using HighwayTolls.Domain.Entities;

namespace HighwayTolls.Application.Interfaces
{
    public interface IPaymentService
    {
        void AddPayment(Payment payment);
    }
}
