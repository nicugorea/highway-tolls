﻿using HighwayTolls.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace HighwayTolls.Application.Interfaces
{
    public interface ITollBoothService
    {
        IEnumerable<TollBooth> GetTollBooths();
        IEnumerable<TollBooth> GetTollBoothsByLocationId(int id);
        TollBooth GetTollBoothById(int id);
    }
}
