﻿using HighwayTolls.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace HighwayTolls.Application.Interfaces
{
    public interface IEmployeeService
    {
        Employee GetEmployeeByIdentityId(string id);
        void AddEmployee(Employee employee);
    }
}
