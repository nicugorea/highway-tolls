﻿using HighwayTolls.Application.Interfaces;
using HighwayTolls.Domain.Entities;
using HighwayTolls.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HighwayTolls.Application.Implementations
{
    public class TollBoothService : ITollBoothService
    {
        private readonly ITollBoothRepository _tollBoothRepository;

        public TollBoothService(ITollBoothRepository tollBoothRepository)
        {
            _tollBoothRepository = tollBoothRepository;
        }

        public IEnumerable<TollBooth> GetTollBoothsByLocationId(int id)
        {
            return _tollBoothRepository.GetTollBoothsByLocationId(id);
        }

        public IEnumerable<TollBooth> GetTollBooths()
        {
            return _tollBoothRepository.GetAll();
        }

        public TollBooth GetTollBoothById(int id)
        {
            return _tollBoothRepository.GetTollBoothById(id);
        }
    }
}
