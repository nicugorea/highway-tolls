﻿using HighwayTolls.Application.Interfaces;
using HighwayTolls.Domain.Entities;
using HighwayTolls.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HighwayTolls.Application.Implementations
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeService(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public void AddEmployee(Employee employee)
        {
            _employeeRepository.Insert(employee);
        }

        public Employee GetEmployeeByIdentityId(string id)
        {
            return _employeeRepository.GetAll().Where(p => p.IdentityUserId == id).FirstOrDefault();
        }
    }
}
