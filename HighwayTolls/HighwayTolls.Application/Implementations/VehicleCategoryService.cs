﻿using HighwayTolls.Application.Interfaces;
using HighwayTolls.Domain.Entities;
using HighwayTolls.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace HighwayTolls.Application.Implementations
{
    public class VehicleCategoryService : IVehicleCategoryService
    {
        private readonly IVehicleCategoryRepository _vehicleCategoryRepository;

        public VehicleCategoryService(IVehicleCategoryRepository vehicleCategoryRepository)
        {
            _vehicleCategoryRepository = vehicleCategoryRepository;
        }

        public IEnumerable<VehicleCategory> GetVehicleCategories()
        {
            return _vehicleCategoryRepository.GetAll();
        }
    }
}
