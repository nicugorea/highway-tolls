﻿using HighwayTolls.Application.Interfaces;
using HighwayTolls.Domain.Entities;
using HighwayTolls.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HighwayTolls.Application.Implementations
{
    public class PriceService : IPriceService
    {
        private readonly IPriceRepository _priceRepository;

        public PriceService(IPriceRepository priceRepository)
        {
            _priceRepository = priceRepository;
        }

        public IEnumerable<Price> GetPrices()
        {
            return _priceRepository.GetAll();
        }

        public IEnumerable<Price> GetPricesByLocationId(int id)
        {
            return _priceRepository.GetPricesByLocationId(id);
        }
    }
}
