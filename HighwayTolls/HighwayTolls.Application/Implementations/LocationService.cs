﻿using HighwayTolls.Application.Interfaces;
using HighwayTolls.Domain.Entities;
using HighwayTolls.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HighwayTolls.Application.Implementations
{
    public class LocationService : ILocationService
    {
        private readonly ILocationRepository _locationRepository;

        public LocationService(ILocationRepository locationRepository)
        {
            _locationRepository = locationRepository;
        }

        public Location GetLocationById(int id)
        {
            return _locationRepository.GetLocationById(id);
        }

        public IEnumerable<Location> GetLocations()
        {
            return _locationRepository.GetAll();
        }
    }
}
