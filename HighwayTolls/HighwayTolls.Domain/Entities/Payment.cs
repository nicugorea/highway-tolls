﻿using System;
using System.Collections.Generic;

namespace HighwayTolls.Domain.Entities
{
    public partial class Payment
    {
        public int Id { get; set; }
        public string EmployeeId { get; set; }
        public int LocationId { get; set; }
        public int VehicleId { get; set; }
        public int TollBoothId { get; set; }
        public decimal Price { get; set; }
        public decimal Paid { get; set; }
        public DateTime Time { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual Location Location { get; set; }
        public virtual TollBooth TollBooth { get; set; }
        public virtual VehicleCategory Vehicle { get; set; }
    }
}
