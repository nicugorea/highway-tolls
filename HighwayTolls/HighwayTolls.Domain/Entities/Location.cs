﻿using System;
using System.Collections.Generic;

namespace HighwayTolls.Domain.Entities
{
    public partial class Location
    {
        public Location()
        {
            Payments = new HashSet<Payment>();
            Prices = new HashSet<Price>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<TollBooth> TollBooths { get; set; }
        public virtual ICollection<Payment> Payments { get; set; }
        public virtual ICollection<Price> Prices { get; set; }
    }
}
