﻿using System;
using System.Collections.Generic;

namespace HighwayTolls.Domain.Entities
{
    public partial class TollBooth
    {
        public TollBooth()
        {
            Payments = new HashSet<Payment>();
        }

        public int Id { get; set; }
        public int LocationId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }

        public virtual Location Location { get; set; }
        public virtual ICollection<Payment> Payments { get; set; }
    }
}
