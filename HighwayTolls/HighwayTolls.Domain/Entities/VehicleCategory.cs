﻿using System;
using System.Collections.Generic;

namespace HighwayTolls.Domain.Entities
{
    public partial class VehicleCategory
    {
        public VehicleCategory()
        {
            Payments = new HashSet<Payment>();
            Prices = new HashSet<Price>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Payment> Payments { get; set; }
        public virtual ICollection<Price> Prices { get; set; }
    }
}
