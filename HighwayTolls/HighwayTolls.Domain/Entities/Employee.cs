﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace HighwayTolls.Domain.Entities
{
    public partial class Employee
    {
        public Employee()
        {
            Payments = new HashSet<Payment>();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Id { get; set; }
        public string IdentityUserId { get; set; }

        public virtual IdentityUser IdentityUser { get; set; }
        public virtual ICollection<Payment> Payments { get; set; }
    }
}
