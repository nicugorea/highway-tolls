﻿using System;
using System.Collections.Generic;

namespace HighwayTolls.Domain.Entities
{
    public partial class Price
    {
        public int LocationId { get; set; }
        public int VehicleCategoryId { get; set; }
        public decimal PriceValue { get; set; }

        public virtual Location Location { get; set; }
        public virtual VehicleCategory VehicleCategory { get; set; }
    }
}
